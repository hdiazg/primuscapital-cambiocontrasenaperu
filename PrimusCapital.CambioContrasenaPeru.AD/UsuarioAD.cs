﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.CambioContrasena.ActiveDirectory
{
    public class UsuarioAD
    {
        public string Username { get; set; }
        public int Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NombreCompleto { get; set; }
        public bool Logueado { get; set; }
        public bool PermitidoChile { get; set; }
        public bool PermitidoPeru { get; set; }
        public string UsernameChile { get; set; }
        public string UsernamePeru { get; set; }
        public string NuevaContrasena { get; set; }

        public string MensajeDetalleError { get; set; }
        public string MensajeErrorAccesoChile { get; set; }
        public string MensajeErrorAccesoPeru { get; set; }

    }
}
