﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.Protocols;
using System.Collections;

namespace PrimusCapital.CambioContrasena.ActiveDirectory
{
    public class ActiveDirectoryService
    {
        public UsuarioAD UsuarioValido(string username, string password)
        {
            try
            {
                #region[Metodo con PrincipalContext Funcionando]
                var usuarioConDetalles = new UsuarioAD();
                PrincipalContext ctxPeru = new PrincipalContext(ContextType.Domain, "192.168.1.200", "DC=PRIMUSCAPITALSAC,DC=local", username, password);
                if (ctxPeru.ValidateCredentials(username, password))
                {    
                    var userPeru = UserPrincipal.FindByIdentity(ctxPeru, IdentityType.SamAccountName, username);
                    var usuarioPeru = new UsuarioAD();
                    usuarioConDetalles.UsernamePeru = userPeru.Context.UserName;
                    usuarioPeru.NombreCompleto = userPeru.DisplayName;
                    usuarioConDetalles.Logueado = true;
                    usuarioConDetalles.PermitidoPeru = true;
                }
                else
                {
                    usuarioConDetalles.Logueado = false;
                    usuarioConDetalles.PermitidoPeru = false;

                    usuarioConDetalles.MensajeErrorAccesoPeru = "Usuario o contraseña Incorrecta! Perú";
                }

                return usuarioConDetalles;

                #endregion

                return new UsuarioAD() { MensajeDetalleError = "Usuario o contraseña Incorrectos!" };
            }
            catch (Exception ex)
            {
                return new UsuarioAD() { MensajeDetalleError = string.Format("ERROR : {0}", ex.Message) };   
            }
        }

        public UsuarioAD ModificaPassword(string username, string password, string newPassword)
        {
            try
            {
                var usuarioConDetalles = new UsuarioAD();
                //Modifica Perú!
                PrincipalContext ctxAdmPeru = new PrincipalContext(ContextType.Domain, "192.168.1.200", "DC=PRIMUSCAPITALSAC,DC=local", ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);

                var userPeru = UserPrincipal.FindByIdentity(ctxAdmPeru, IdentityType.SamAccountName, username);
                usuarioConDetalles.PermitidoPeru = true;

                userPeru.UnlockAccount();
                userPeru.RefreshExpiredPassword();

                //NuevaContraseña.-
                userPeru.SetPassword(newPassword);
                //Cambio Contraseña.-
                //user.ChangePassword(password, newPassword);

                //Guarda Contraseña.-
                userPeru.Save();
                userPeru.Dispose();

                return usuarioConDetalles;
            }
            catch (Exception ex)
            {
                return new UsuarioAD() { MensajeDetalleError = string.Format("ERROR : {0}", ex.Message) };
            }
        }

        private UsuarioAD ObtenerDetallesUsuario(PrincipalContext ctx, string userName, string password)
        {
            List<UserPrincipal> searchPrinciples = new List<UserPrincipal>();
            searchPrinciples.Add(new UserPrincipal(ctx) { DisplayName = String.Format("*{0}*", userName) });

            List<Principal> results = new List<Principal>();
            var searcher = new PrincipalSearcher();

            IList<UsuarioAD> userList = new List<UsuarioAD>();

            foreach (var item in searchPrinciples)
            {
                searcher = new PrincipalSearcher(item);
                foreach (Principal found in searcher.FindAll())
                {
                    DirectoryEntry directoryEntry = (DirectoryEntry)found.GetUnderlyingObject();
                    PropertyValueCollection fotoAD = directoryEntry.Properties["thumbnailPhoto"];
                    byte[] fotoEnBytes = new byte[0];

                    if (fotoAD.Value != null && fotoAD.Value is byte[])
                    {
                        fotoEnBytes = (byte[])fotoAD.Value;
                    }

                    UsuarioAD usuarioActiveDirectory = new UsuarioAD();

                    var gruposEsteUsuario = found.GetGroups().ToList();

                    UserPrincipal user = ((UserPrincipal)found);
                    usuarioActiveDirectory.Username = user.SamAccountName;
                    usuarioActiveDirectory.Nombre = user.GivenName;
                    usuarioActiveDirectory.Apellido = user.Surname;

                    userList.Add(usuarioActiveDirectory);
                    break;
                }
            }

            return userList.First();
        }


    }
}
