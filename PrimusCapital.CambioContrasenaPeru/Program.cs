﻿using PrimusCapital.CambioContrasena.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.CambioContrasenaPeru
{
    class Program
    {
        static void Main(string[] args)
        {
            bool continuar = true;
            bool salir = true;

            while (continuar)
            {
                var valor = "";
                var valid = false;
                while (!valid)
                {
                    MostrarMenu();
                    Console.Write("* - Ingrese una opcion : ");

                    valor = Console.ReadLine();
                    valid = !string.IsNullOrWhiteSpace(valor) && valor.All(v => v >= '0' && v <= '4');
                    if (!valid)
                    {
                        WriteError("\n   OPCION NO ENCONTRADA...        ");
                        Console.WriteLine("\nPresione una Tecla para continuar...");
                        Console.ReadKey();
                    }
                }
                int opcion = int.Parse(valor);
                if (opcion != 1 & opcion != 2 & opcion != 3)
                {
                    WriteError("\n*    OPCION NO ENCONTRADA...      *");
                    Console.WriteLine("\nPresione una Tecla para continuar...");
                    Console.ReadKey();
                }

                switch (opcion)
                {
                    case 1:
                        TituloValidaUsuario();

                        var validUser = false;
                        var user = string.Empty;
                        while (!validUser)
                        {
                            Console.Write("Ingrese Usuario    :");
                            user = Console.ReadLine();
                            if (user == "administrador" || user == "Administrador" || user == "administrator" || user == "Administrator")
                            {
                                validUser = false;

                            }
                            else
                            {
                                validUser = true;
                            }
                            if (!validUser)
                            {
                                var valorvalid = validUser;
                                MensajeErrorValidaUserAdmin();
                                Console.WriteLine("\nPresione una Tecla para continuar...");
                                Console.ReadKey();

                                Console.Clear();
                                TituloValidaUsuario();
                            }
                        }
                        
                        Console.Write("Ingrese Contraseña :");
                        string pass = Console.ReadLine();

                        VerificaAcceso(user, pass);

                        Console.WriteLine("\nPresione una Tecla para regresar...");
                        Console.ReadKey();
                        
                        break;
                    case 2:

                        TituloModificaContrasena();
                        validUser = false;
                        user = string.Empty;
                        while (!validUser)
                        {
                            Console.Write("Ingrese Usuario    :");
                            user = Console.ReadLine();
                            if (user == "administrador" || user == "Administrador" || user == "administrator" || user == "Administrator")
                            {
                                validUser = false;

                            }
                            else
                            {
                                validUser = true;
                            }
                            if (!validUser)
                            {
                                var valorvalid = validUser;
                                MensajeErrorValidaUserAdmin();
                                Console.WriteLine("\nPresione una Tecla para continuar...");
                                Console.ReadKey();

                                Console.Clear();
                                TituloModificaContrasena();
                            }
                        }
                        
                        Console.Write("Ingrese Contraseña Nueva :");
                        string newPass = Console.ReadLine();

                        ActualizacionUsuario(user, null, newPass);

                        Console.WriteLine("\nPresione una Tecla para regresar...");
                        Console.ReadKey();
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }
        #region[Titulos]
        private static void TituloValidaUsuario()
        {
            Console.Clear();
            WriteContainer("****************************************************");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("   Muestra si esta correcto el inicio de sesion   ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*");
            WriteContainer("****************************************************");
            Console.WriteLine("");
        }
        private static void TituloModificaContrasena()
        {
            Console.Clear();
            WriteContainer("*************************************************");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            //Console.Write("   Modificación Contraseña para Aplicaciones   ");
            Console.Write("            Modificación Contraseña            ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*");
            WriteContainer("*************************************************");
            Console.WriteLine("");
        }
        #endregion

        private static void MensajeErrorValidaUserAdmin()
        {
            WriteError("*********************************************");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("      No Puede Cambiar Esta Contraseña     ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("*");
            WriteError("*********************************************");
            Console.WriteLine("");
        }

        private static void MostrarMenu()
        {
            Console.Clear();
            WriteContainer("************************************");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("   [1] - Verificar cuenta         ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*");

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("   [2] - Cambio Contraseña        ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*");

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("   [3] - Salir                    ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*");
            WriteContainer("************************************");
            Console.WriteLine("");
        }

        private static void VerificaAcceso(string username, string password)
        {
            Console.WriteLine("");
            Console.WriteLine("Buscando...");
            Console.WriteLine("");
            try
            {
                var ad = new ActiveDirectoryService();
                var respuesta = ad.UsuarioValido(username, password);

                if (respuesta.PermitidoPeru)
                {
                    WriteSuccess("****************************************");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("*");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("      Usuario Encontrado en Perú      ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("*");
                    WriteSuccess("****************************************");
                    Console.WriteLine("");
                }
                else
                {
                    WriteError("*********************************************");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("*   ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(respuesta.MensajeErrorAccesoPeru);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("   *");
                    WriteError("*********************************************");
                    Console.WriteLine("");
                }
            }
            catch (Exception)
            {
                WriteError("Error");
            }
            Console.WriteLine();
        }

        private static void ActualizacionUsuario(string username, string oldPass, string newPass)
        {
            Console.WriteLine("");
            Console.WriteLine("Modificando...");
            Console.WriteLine("");
            try
            {
                var ad = new ActiveDirectoryService();
                var respuesta = ad.ModificaPassword(username, oldPass, newPass);

                if (respuesta.PermitidoPeru)
                {
                    WriteSuccess("****************************************");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("*");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("        Contraseña Actualizada        ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("*");
                    WriteSuccess("****************************************");
                    Console.WriteLine("");
                }
                else
                {
                    WriteError(respuesta.MensajeDetalleError);
                }
            }
            catch (Exception)
            {
                WriteError("Error");
            }
            Console.WriteLine();
        }


        private static bool PreguntarSiDeseaSalir()
        {
            Console.WriteLine("");
            Console.WriteLine("********************");
            Console.WriteLine("*   Desea Salir?   *");
            Console.WriteLine("********************");
            Console.WriteLine("*     [1] - Si     *");
            Console.WriteLine("*     [2] - No     *");
            Console.WriteLine("********************");
            var opcion = int.Parse(Console.ReadLine());
            if (opcion != 1 & opcion != 2)
            {
                return PreguntarSiDeseaSalir();
            }
            if (opcion == 1)
            {
                Environment.Exit(0);
            }

            return opcion.Equals(2);
        }

        //En caso de que el envio sea Ok
        static void WriteSuccess(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        //En caso de que ocurra un Error
        static void WriteError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        static void WriteContainer(string message)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
